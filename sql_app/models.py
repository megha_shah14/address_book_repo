from sqlalchemy import Column, Integer
from .database import Base


class Addressnew(Base):
    __tablename__ = "addressnew"

    id = Column(Integer, primary_key=True, index=True)
    lat = Column(Integer, unique=True, index=True)
    long = Column(Integer, unique=True, index=True)
    user_id = Column(Integer, unique=True, index=True)
