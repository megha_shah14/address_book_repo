from typing import Optional
from pydantic import BaseModel


class AddressnewBase(BaseModel):
    lat: int
    long: int


class AddressnewCreate(AddressnewBase):
    pass

class AddressnewUpdate(AddressnewBase):
    pass

class AddressnewDelete(BaseModel):
    lat: Optional[int] = None
    long: Optional[int] = None
    id: Optional[int] = None
    user_id: Optional[int] = None


class Addressnew(AddressnewBase):
    id: int
    user_id: int

    class Config:
        orm_mode = True