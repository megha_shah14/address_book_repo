from typing import List
from fastapi import Depends, FastAPI, HTTPException, Request, Response

from sqlalchemy.orm import Session
from .database import SessionLocal, engine
from . import crud, models, schemas


models.Base.metadata.create_all(bind=engine)
app = FastAPI()

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/create/address/{user_id}", response_model=schemas.Addressnew)
def create_address(user_id: int, addressnew: schemas.AddressnewCreate, db: Session = Depends(get_db)):

    """ Create address for the given user """

    db_address = crud.get_user_address(db, user_id=user_id)
    if db_address:
        raise HTTPException(status_code=500, detail="Address for this user already registered")

    error_msg = crud.validate_address(addressnew.lat, addressnew.long)
    if error_msg:
        raise HTTPException(status_code=500, detail=error_msg)
    return crud.create_address(db=db, addressnew=addressnew, user_id=user_id)


@app.get("/address/", response_model=List[schemas.Addressnew])
def read_address(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):

    """ Show all the address """

    address = crud.get_address(db, skip=skip, limit=limit)
    return address


@app.get("/address/{user_id}", response_model=schemas.Addressnew)
def read_address_for_user(user_id: int, db: Session = Depends(get_db)):

    """ Show address for the given user """

    db_address = crud.get_user_address(db, user_id=user_id)
    if db_address is None:
        raise HTTPException(status_code=404, detail="Address not found for this user")
    # db_address = crud.get_user_address(db, user_id=user_id)

    return db_address


@app.put("/edit/address/{user_id}", response_model=schemas.Addressnew)
def update_address(user_id: int, addressnew: schemas.AddressnewUpdate, db: Session = Depends(get_db)):

    """ Update address for the given user """

    db_address = crud.get_user_address(db, user_id=user_id)
    if db_address is None:
        raise HTTPException(status_code=500, detail="Can't be edited as address for this user is not registered")

    error_msg = crud.validate_address(addressnew.lat, addressnew.long)
    if error_msg:
        raise HTTPException(status_code=500, detail=error_msg)
    return crud.edit_address(db=db, db_address=db_address, addressnew=addressnew)


@app.delete("/delete/address/{user_id}", response_model=schemas.AddressnewDelete)
def update_address(user_id: int, addressnew: schemas.AddressnewDelete, db: Session = Depends(get_db)):

    """ Delete address for the given user """

    db_address = crud.get_user_address(db, user_id=user_id)
    if db_address is None:
        raise HTTPException(status_code=500, detail="Can't be deleted as address for this user is not registered")

    return crud.delete_address(db=db, db_address=db_address)