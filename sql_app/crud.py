from sqlalchemy.orm import Session
from . import models, schemas


def get_address(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Addressnew).offset(skip).limit(limit).all()


def create_address(db: Session, addressnew: schemas.AddressnewCreate, user_id: int):
    db_address = models.Addressnew(**addressnew.dict(), user_id=user_id)
    db.add(db_address)
    db.commit()
    db.refresh(db_address)
    return db_address


def edit_address(db: Session, db_address: schemas.Addressnew, addressnew: schemas.AddressnewUpdate):
    db_address.lat = addressnew.lat
    db_address.long = addressnew.long
    db.add(db_address)
    db.commit()
    db.refresh(db_address)
    return db_address


def delete_address(db: Session, db_address: schemas.Addressnew):
    db.delete(db_address)
    db.commit()
    return {"ok": True}


def get_user_address(db: Session, user_id: int):
    return db.query(models.Addressnew).filter(models.Addressnew.user_id == user_id).first()


def validate_address(lat, long):
    msg = ""
    if (lat < -90 or lat > 90):
        msg = "Latitude must be between -90 and 90 degrees inclusive."

    elif (long < -180 or long > 180):
        msg = "Longitude must be between -180 and 180 degrees inclusive."
    return msg